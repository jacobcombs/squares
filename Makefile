main = final-project

$(main).pdf : *.tex *.bib
	pdflatex $(main) && \
	bibtex $(main) && \
	pdflatex $(main) && \
	pdflatex $(main) && \
	pdflatex $(main)

clean :
	rm -f \
	$(main).aux \
	$(main).bbl \
	$(main).blg \
	$(main).log \
	$(main).out \
	$(main).toc \
	$(main).pdf

fresh : clean $(main).pdf

draft : 
	pdflatex $(main)
