
// Enable functions to operate on d3 selections with property syntax
d3.selection.prototype.callReturn = function(callable) {
    return callable(this);
};

var svgWidth = 900;
var svgHeight = 300;
var dataByClass = d3.nest()
    .key(d => d.True)
    .entries(data);
var classes = dataByClass.map(d => d.key);
var numClasses = classes.length;

var xScale = d3.scaleLinear().domain([0, numClasses]).range([0, svgWidth]);
var yScale = d3.scaleLinear().domain([0, 1]).range([0, svgHeight]);
var histogramWidth = xScale(1);
var xHistogramScale = d3.scaleLinear().domain([0, 1]).range([0, histogramWidth]);

function createSvg(sel) {
    return sel
        .append("svg")
        .attr("width", svgWidth)
        .attr("height", svgHeight);
}

// todo: un-hardcode top and bottom coords of axis
// (which must match top and bottom coords of the center rules
// for each histogram)
var scorePositionScale = d3.scaleLinear().domain([0.0, 1.0]).range([yScale(0.9), yScale(0.1)]);
var axis = d3.axisLeft(scorePositionScale).ticks(10);

// todo: label this axis "Prediction" or "Score" or something
// todo: make axis NOT overlap with the first histogram
function drawAxis(sel) { // add axis, return new group below it
    sel.append("g")
        .attr("transform", "translate(30,0)")
        .call(axis);
    return sel.append("g");
}

function createHistogramGroups(dataByClass) {
    return function(sel) {
        return sel
            .selectAll("g")
            .data(dataByClass)
            .enter()
            .append("g")
            .attr("transform", function(d) {
                return "translate(" + xScale(classes.indexOf(d.key)) + ",0)";
            })
            .attr("id", function(d) {return "histogram-" + d.key;})
    };
}

function drawHistogramRules(sel) { // add paths, return parent
    sel.append("path")
        .attr("d", "M" + xHistogramScale(0.5) + "," + yScale(0.1) + "L" + xHistogramScale(0.5) + "," + yScale(0.9))
        .attr("stroke", "black");
    return sel;
}

function drawHistogramLabel(sel) { // add text, return parent
    sel.append("text")
        .text(d => d.key)
        .attr("x", xHistogramScale(0.5))
        .attr("y", yScale(1.0))
        .attr("text-anchor", "middle");
    return sel;
}

var scoreScale = d3.scaleLinear().range([0.0, 1.0]);

function drawPositiveBins(sel) { // and return parent
    // this function has a bug. In particular the data associated
    // with the selection at the point it is called is an
    // Array[3], when it sould be a single element of that array.
    // Since the sel is intended to be the group element with
    // which each array element is associated, I'm not sure why
    // this is happening...
    bins = d3.histogram()
        .domain([0.0, 1.0])
        .thresholds(scoreScale.ticks(10))
        (function(d) {
            return d.values[d.key]; // get values labeled this class
        });
    lengthScale = d3.scaleLinear()
        .domain([0, d3.max(bins, d => d.length)])
        .range([0, xHistogramScale(0.9) - xHistogramScale(0.5)]);
    // todo: length needs to be consistent across classes

    // idea to create new groups by class .bar is taken from an example by
    // M Bostock https://bl.ocks.org/mbostock/3048450
    bar = sel.selectAll(".bar")
        .data(bins)
        .enter()
        .append("g")
        .attr("class", "bar")
        .attr("transform", function(d) { return "translate(" + xHistogramScale(0.5) + "," + scorePositionScale(d.x1) + ")"; });
    // for each bar, make the actual rect object
    bar.append("rect")
        .attr("height", scorePositionScale(0.0) - scorePositionScale(0.1) - 1) // a little space b/w bars
        //.attr("width", d => lengthScale(d.length));
        .attr("width", 50); // at least it draws bars...
    return sel;
}

d3.select("#histograms")
    .callReturn(createSvg)
    .callReturn(drawAxis)
    .callReturn(createHistogramGroups(dataByClass))
    .callReturn(drawPositiveBins)
    .callReturn(drawHistogramRules)
    .callReturn(drawHistogramLabel);

d3.select("#table")
    .callReturn(createSvg);